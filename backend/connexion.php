<?php
    // Database connection parameters.
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'todolist');
define('DB_HOST', '127.0.0.1');
define('DB_PORT', '3306');


try{
    $db=new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME."; port=".DB_PORT, DB_USER, DB_PASS);
}
catch(PDOException $e){
    die("Erreur ". $e->getMessage());
}
?>